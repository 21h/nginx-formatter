'''
Nginx configuration files beautifuler
Copyright by Vladimir Smagin, 2016
21h@blindage.org   http://blindage.org
Licensed under GPL 3

Usage example: python3 ./nginx_format.py blindage.org > blindage.org.out

DO NOT OUTPUT RESULTS TO INPUT FILES!

'''

import re, sys

if len(sys.argv) != 2:
    print('Need filename as parameter!')
    sys.exit(1)

config_in = sys.argv[1]
config_out = config_in + ".out"

cursor_indent_spaces = "    "
cursor_indent = 0

contents = ""

config_file = open(config_in, 'r')
for line in config_file:
    line = re.sub('((?!\n)\s)+', ' ', line)
    line = line.strip(" \t\r")
    if len(line.strip())==0: continue
    if line.find('#', 0) < 0:
        line = line.replace("\n", "")
        line = line.replace(";", ";\n")
    #else:
        #line = line.replace(";", ";\n")
        #line += "\n"
    contents += line

#contents = contents.replace('\t', ' ')
#contents = re.sub('((?!\n)\s)+', ' ', contents)
contents = contents.replace('{', '{\n')
contents = contents.replace('}', '}\n')
#print(contents)
contents = contents.split('\n')

for pos, line in enumerate(contents):
    if line.find('}', 0) >= 0:
        cursor_indent -= 1
        contents[pos] = contents[pos] + "\n"
    for i in range(0, cursor_indent):
        contents[pos] = cursor_indent_spaces + contents[pos]
    if line.find('{', 0) >= 0:
        cursor_indent += 1
        contents[pos] = "\n" + contents[pos]

print("\n".join(contents))
